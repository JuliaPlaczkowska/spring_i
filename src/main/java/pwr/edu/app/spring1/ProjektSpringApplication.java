package pwr.edu.app.spring1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektSpringApplication.class, args);
		System.out.println("Hello World!");
	}

}
